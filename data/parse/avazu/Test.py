import pickle
import numpy as np

#列表中的每一个元素，序号+list for codes + list for date + list for numberic
#data=np.array(pickle.load(open("data_test.pkl",'rb')))
#print(data)

#应该是病人和visit序列
temp=pickle.load(open("data_test_3digit.pkl",'rb'))
print(temp.shape)

data_3digit=np.array(temp)
print(data_3digit)

#病人和01的映射
target=np.array(pickle.load(open("target_test.pkl",'rb')))
print(target)


#序号和对应code的映射，== 943: 'Time of visit'
dictionary=np.array(pickle.load(open("dictionary_3digit.pkl",'rb')))
print(dictionary)

